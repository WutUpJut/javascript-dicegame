// Variables
var yourScore = 0;
var compScore = 0;
var yourDice = 0;
var compDice = 0;

//Button Click
document.getElementById("roll").onclick = function() {RollDice()};

function RollDice() {
    //Player Dice Value
    yourDice = Math.floor(Math.random() * (7 - 1) + 1);
    document.getElementById("player-dice").src = "dice-images/dice-" + yourDice +".png";
    //Computer Dice Value
    compDice = Math.floor(Math.random() * (7 - 1) + 1);
    document.getElementById("computer-dice").src = "dice-images/dice-" + compDice +".png";
    //Check Dice Values
    if(yourDice != compDice){
        //Add to Scores()
        yourScore += yourDice;
        compScore += compDice;
        //Display Values
        document.getElementById("player-score").textContent = yourScore;
        document.getElementById("computer-score").textContent = compScore;
    }
    if(yourScore >= 50 || compScore >= 50){
        PlayerWins();
    }
}

function PlayerWins() {
    document.getElementById("roll").textContent = "Click to Play Again!";
    document.getElementById("roll").onclick = function() {Clear()};
}

function Clear() {
    //Player Dice Value
    yourDice = 1;
    document.getElementById("player-dice").src = "dice-images/dice-" + yourDice +".png";
    //Computer Dice Value
    compDice = 1;
    document.getElementById("computer-dice").src = "dice-images/dice-" + compDice +".png";
    //Scores
    yourScore = 0;
    compScore = 0;
    //Display Values
    document.getElementById("player-score").textContent = yourScore;
    document.getElementById("computer-score").textContent = compScore;
    //Set Button Listener Back
    document.getElementById("roll").onclick = function() {RollDice()};
}